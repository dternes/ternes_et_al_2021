# Ternes_et_al_2021

This folder contain the code specific to the paper. Typically this will be a script that reproduces the results of the paper. All functions used by that script should be integrated into the appropriate folder, or a new folder for those functions created as necessary.

