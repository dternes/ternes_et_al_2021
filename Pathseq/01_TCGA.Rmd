---
title: "midica16"
author: "Aurélien Ginolhac"
date: '`r format(Sys.Date(), "%d/%m/%Y")`'
output:
  pdf_document: default
  html_document: default
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = FALSE)
library(tidyverse)
```

### workflow


- extract unmapped reads using SAM flag `4`
- run mOTU.pl with default parameters


### connect to gaia

```{r}
bifag::cluster_mount("gaia")
```


### load either abundances or counts

```{r}
type <- "counts"
stopifnot(type %in% c("counts", "abundances"))

unmapped <- "/Users/aurelien.ginolhac/gaia/midica16/unmapped/"

motu_files <- Sys.glob(paste0(unmapped, "?/*/RESULTS/mOTU.", type , ".gz"))
folders <- Sys.glob(paste0(unmapped, "?/*-*/"))

ids <- str_split_fixed(motu_files, "/", n = 10)[, 8]
motu_results <- map_df(set_names(motu_files, nm = ids), ~ read_tsv(.x, skip = 1,
                                                                 col_names = c("ID", type),
                                                                 col_types = "cd"), .id = "file")
```


loaded `r length(motu_files)` samples with some reads left after trimming out `r length(folders)` (`r scales::percent(length(motu_files)/length(folders))`) samples treated.

The total number of BAMs for TCGA-COAD and TCGA-READ is 2636.

### fetch stats on trimmed reads

```{r}
stat_files <- Sys.glob(paste0(unmapped, "?/*/motus.processing.1/stats/lane1.fq.gz.raw.reads.stats"))
# refetch the ids as here we got them all and not a subset like for mOTU results
stat_ids <- str_split_fixed(stat_files, "/", n = 10)[, 8]
stat_results <- map_df(set_names(stat_files, nm = stat_ids), ~ read_tsv(.x, col_names = TRUE,
                                                                            col_types = "dd"), .id = "file")
```


### Stats on unmapped reads

```{r, fig.width = 9}
stat_results %>%
  mutate(length = Bases / Reads,
         Bases_log = log(Bases),
         Reads_log = log(Reads)) %>%
  select(-Bases, -Reads) %>%
  gather(stat, value, -file) %>%
  ggplot(aes(x = value)) +
  geom_histogram(bins = 30) +
  facet_wrap(~ stat, scales = "free") +
  theme_bw(14)
```


### load annotations

assuming same file for all abundances

```{r}
anno <- file.path(unmapped, "0/0c02bf18-3f95-468a-bdb8-408ad4e77e6a_7b2e17b4-e64e-4b19-96b6-2f5c8d7a30f3_gdc_realn_rehead/RESULTS//mOTU-LG.v1.annotations.txt") %>%
  read_tsv(col_types = cols())
```

### annotate abundances

```{r}
mOTU <- inner_join(motu_results, anno, by = "ID")
```


unique ids, `r mOTU$file %>% unique %>% length` out of `r length(motu_files)` (`r scales::percent((mOTU$file %>% unique %>% length)/length(motu_files))`)

and `r scales::percent((mOTU$file %>% unique %>% length)/length(folders))` of the number of bams

```{r}
tibble(unique = mOTU$file %>% unique) %>% pander::pander()
```


### loading clinical


#### from GDC

no correspondences between sha1 tags in the manifest and clinical data

```{r, warning = FALSE}
clinical <- jsonlite::fromJSON("data/clinical.project-TCGA-COAD.2017-02-10T08-13-02.213876.json")

# from Eric
clinical %>%
  select(diagnoses, case_id) %>%
  mutate(diagnoses = map(diagnoses, as_tibble)) %>%
  unnest() -> diagnos
#clinical$demographic_lst <- as.list(clinical$demographic) %>% head
#  select(demographic, case_id) %>%
#  mutate(demographic2 = as.list(demographic)) %>%
#  mutate(demographic_n = map_int(demographic, nrow)) %>%
#  unnest() -> demographic
clinical %>%
  select(exposures, case_id) %>%
  mutate(exposures = map(exposures, as_tibble)) %>%
  unnest() -> exposur
# "submitter_id" differ
un_clinical <- inner_join(diagnos, exposur, by = c("case_id", "state", "updated_datetime", "created_datetime"))

# FIXME the demographic info is not a proper list-column


#clinical %>%
#  select(exposures, case_id) %>%
#  mutate(exp_n = map_int(exposures, length)) %>%
#  filter(exp_n == 0)
#mOTU %>%
#  separate(file, c("case_id", "bam"), sep = "_") %>% write_tsv("results/mOTU.tsv")
```

lost 4 ids were clinical data are missing

```{r}
clinical %>%
  select(diagnoses, case_id) %>%
  mutate(diag_n = map_int(diagnoses, length)) %>%
  filter(diag_n == 0) %>% pander::pander()
```

### loading metadata cart

for TCGA-COAD and TCGA-READ

```{r}
meta_cart <- jsonlite::fromJSON("data/metadata.cart.2017-03-09T14-35-25.717702.json")

meta_cart %>%
  select(cases, file_id) %>%
  mutate(submitter_id = map_chr(cases, "submitter_id"),
         samples = map(cases, "samples") %>% flatten(),
         sample_type = map_chr(samples, "sample_type"),
         case_id = map_chr(cases, "case_id")) %>%
  select(-cases, -samples) -> coad_cases

mOTU %>%
 separate(file, c("file_id", "file_name"), sep = "_", extra = "merge") %>%
  inner_join(coad_cases, by = "file_id") %>%
  left_join(un_clinical, by = "case_id") -> mOTU_clinical
```




```{r, eval = FALSE}
### laoding biospecimen
bio <- jsonlite::fromJSON("data/biospecimen.project-TCGA-COAD.2017-02-10T08-28-07.781688.json") %>%
  unnest()


bio %>%
  select(case_id, sample_type) %>%
  inner_join(mOTU_clinical, by = "case_id")
```


### extract taxonomic info until gets missing (motus_linkage)

```{r}
mOTU_clinical %>%
  # melt all taxonomic levels
  gather(taxo, value, c("Superkingdom", "Phylum", "Class", "Order", "Family", "Genus", "SpeciesCluster")) %>%
  # remove when value is missing
  filter(!is.na(value)) %>%
  # per id
  group_by(file_id, ID) %>%
  # extract maximum info per id
  filter(row_number() == n()) -> mOTU_reduced
write_rds(mOTU_reduced, here::here("results", "mOTU_reduced.rds"))
```




```{r, fig.height = 14, fig.width = 11}
mOTU_reduced %>%
  ungroup() %>%
  mutate(submitter_id = sub("TCGA-", "", submitter_id)) %>%
  #mutate(value = forcats::fct_reorder(value, counts)) %>%
  ggplot(aes_string(x = "submitter_id", y = "value", fill = type)) +
  geom_tile() + 
  theme_classic(14) +
  theme(axis.text.y = element_text(face = "italic", colour = "black"),
        panel.grid.major.y = element_line(linetype = "dashed", colour = "grey80"),
        axis.text.x = element_text(angle = 90, size = 8, hjust = 1)) +
  viridis::scale_fill_viridis(direction = -1, option = "plasma") +
  labs(x = NULL,
       y = NULL) -> my_heat
my_heat
ggsave(paste0("taxo_", type, ".pdf"), my_heat, height = 297, width = 210, units = "mm")
```

#### by tissue


```{r, fig.height = 14, fig.width = 11, warning = FALSE}
mOTU_reduced %>%
  ungroup() %>%
  mutate(submitter_id = sub("TCGA-", "", submitter_id)) %>%
  #filter(counts > 2) %>% 
  ggplot(aes_string(x = "submitter_id", y = "value", fill = type)) +
  geom_tile() + 
  theme_classic(14) +
  theme(axis.text.y = element_text(face = "italic", colour = "black"),
        panel.grid.major.y = element_line(linetype = "dashed", colour = "grey80"),
        axis.text.x = element_text(angle = 90, size = 8, hjust = 1)) +
  viridis::scale_fill_viridis(direction = -1, option = "plasma") +
  facet_wrap(~ sample_type, nrow = 1) +
  labs(x = NULL,
       y = NULL) -> heat_tissue
heat_tissue
ggsave(paste0("taxo_", type, "_tissue.pdf"), heat_tissue, height = 297, width = 210, units = "mm")
```




```{r}
mOTU_reduced %>%
  ungroup() %>% 
  select(submitter_id, sample_type) %>%
  distinct() %>%
  count(submitter_id) %>%
  filter(n > 1) %>% pull(submitter_id) -> matched
```


Match samples: `r matched`

```{r, fig.width = 9, fig.height = 7}
mOTU_reduced %>%
  filter(submitter_id %in% matched) %>%
  ggplot(aes_string(x = "sample_type", y = "value", fill = type)) +
  geom_tile() + 
  theme_classic(15) +
  theme(axis.text.y = element_text(face = "italic", colour = "black"),
        panel.grid.major.y = element_line(linetype = "dashed", colour = "grey80"),
        axis.text.x = element_text(angle = 90, size = 8, hjust = 1)) +
  viridis::scale_fill_viridis(direction = -1, option = "plasma") +
  facet_wrap(~ submitter_id, nrow = 1) +
  labs(x = NULL,
       y = NULL) 
```


#### by stage

```{r, fig.height = 14, fig.width = 11, warning = FALSE}
mOTU_reduced %>%
  ungroup() %>%
  mutate(submitter_id = sub("TCGA-", "", submitter_id)) %>%
  filter(counts > 2) %>% 
  mutate(tstage = if_else(is.na(tumor_stage), "not reported", tumor_stage),
         tstage = factor(tstage),
         stage = forcats::fct_collapse(tstage,
                                       "I"  = c("stage i", "stage ia"),
                                       "II" = c("stage iia", "stage iib", "stage ii"),
                                       "III" = c("stage iiic", "stage iiib"),
                                       "IV" = c("stage iv", "stage iva"),
                                       "NA" = c("not reported"))) %>%
  ggplot(aes_string(x = "submitter_id", y = "value", fill = type)) +
  geom_tile() + 
  theme_classic(14) +
  theme(axis.text.y = element_text(face = "italic", colour = "black"),
        panel.grid.major.y = element_line(linetype = "dashed", colour = "grey80"),
        axis.text.x = element_text(angle = 90, size = 8, hjust = 1)) +
  viridis::scale_fill_viridis(direction = -1, option = "plasma") +
  facet_grid(sample_type ~ stage) +
  labs(x = NULL,
       y = NULL) -> heat_stage
heat_stage
ggsave(paste0("taxo_", type, "_stage.pdf"), heat_stage, height = 297, width = 210, units = "mm")
```


```{r}
knitr::knit_exit()
```


### Read stats for positive samples


```{r, fig.width = 11}
mOTU %>% select(file) %>%
  distinct() %>%
  inner_join(stat_results, by = "file") %>%
  mutate(length = Bases / Reads) %>% #,
       #  Bases_log = log(Bases),
        # Reads_log = log(Reads)) %>%
  #select(-Bases, -Reads) %>%
  gather(stat, value, -file) %>%
  ggplot(aes(x = file, y = value)) +
  geom_col() +
  facet_wrap(~ stat, scales = "free") +
  labs(x = NULL) +
  theme_bw(14) +
  theme(axis.text.x = element_blank())
```


### relation between bacterial counts and read counts

expecting none

```{r}
stopifnot(type == "counts")
mOTU_reduced %>%
  mutate(file = paste(file_id, file_name, sep = "_")) %>%
  group_by(file) %>% 
  summarise(sum_count = sum(counts)) %>%
  inner_join(stat_results, by = "file") %>%
  ggplot(aes(x = Reads, y = sum_count)) +
  geom_point() +
  scale_x_log10() +
  theme_bw(14)
```

