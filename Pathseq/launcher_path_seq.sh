#!/bin/bash -l
#SBATCH -J midica16
#SBATCH --mail-type=end,fail
#SBATCH --mail-user=aurelien.ginolhac@uni.lu
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 28
#SBATCH --mem=112GB
####SBATCH --ntasks-per-node=1
#SBATCH --time=5-0:00:00
#SBATCH -p batch
#SBATCH --qos=qos-batch

#echo "Hello from the batch queue on node ${SLURM_NODELIST}"
# Your more useful application can be started below!

module load lang/Java


#--input,-I:String             BAM/SAM/CRAM file containing reads  This argument must be specified at least once.
#                              Required. 
#--microbe-bwa-image,-MI:StringMicrobe reference BWA index image file generated using BwaMemIndexImageCreator. If running
#                              on a Spark cluster, this must be distributed to local disk on each node.  Required. 
#--microbe-fasta,-MF:String    Reference corresponding to the microbe reference image file  Required. 
#--scores-output,-SO:String    URI for the taxonomic scores output  Required. 
#--taxonomy-file,-T:String 

export BILOC=/scratch/users/aginolhac/test/

#for f in ~/midica16/gdc-portal/complete/00*/*bam
for f in /scratch/users/aginolhac/test/cad*/*bam
  do echo $f
  id="$(echo $f |  awk -F "/" 'BEGIN{OFS="/"}{print $(NF-1)}')"
  mkdir -p results2/${id}
  srun gatk --java-options "-Xmx110g" \
    PathSeqPipelineSpark \
      --input $f \
      --filter-bwa-image ${BILOC}ftp.broadinstitute.org/bundle/pathseq/pathseq_host.fa.img \
      --min-clipped-read-length 70 \
      --microbe-fasta ${BILOC}ftp.broadinstitute.org/bundle/pathseq/pathseq_microbe.fa \
      --microbe-bwa-image ${BILOC}ftp.broadinstitute.org/bundle/pathseq/pathseq_microbe.fa.img \
      --taxonomy-file ${BILOC}ftp.broadinstitute.org/bundle/pathseq/pathseq_taxonomy.db \
      --output results2/${id}/`basename $f` \
      --scores-output results2/${id}/`basename $f .bam`.pathseq.txt
done

