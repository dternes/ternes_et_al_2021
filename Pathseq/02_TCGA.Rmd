---
title: "PathSeq GATK"
author: "Aurelien Ginolhac"
date: "1/28/2019"
output:
  pdf_document: default
  html_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
library(tidyverse)
#bifag::cluster_mount(cluster = "iris")
```

assuming installing GATK is fine.

## indexed human and microbe data

40Gb downloaded and available here:

`/scratch/users/aginolhac/160503/ftp.broadinstitute.org/`

## run script

`run_pathseq.sh` takes one argument: a BAM file.

The expected human reference is **GRCh38** (hg20).

```{bash echo=TRUE, eval=FALSE}
#!/usr/bin/env bash

echo ${1}
[[ -s "$1" ]] || exit 1

id="$(echo ${1} |  awk -F "/" 'BEGIN{OFS="/"}{print $(NF-1)}')"
BASEDIR=$SCRATCH/160503/
TARGET=$BASEDIR/results/${id}
INPUTS=$BASEDIR/inputs/
mkdir -p ${TARGET} $INPUTS

[[ -s ${TARGET}/`basename ${1} .bam`.pathseq.txt ]] && exit 0

rsync -aq "${1}" "${INPUTS}/"

gatk --java-options "-Xmx112g" \
    PathSeqPipelineSpark \
      --input ${INPUTS}/`basename ${1}` \
      --filter-bwa-image $BASEDIR/ftp.broadinstitute.org/bundle/pathseq/pathseq_host.fa.img \
      --min-clipped-read-length 70 \
      --microbe-fasta $BASEDIR/ftp.broadinstitute.org/bundle/pathseq/pathseq_microbe.fa \
      --microbe-bwa-image $BASEDIR/ftp.broadinstitute.org/bundle/pathseq/pathseq_microbe.fa.img \
      --taxonomy-file $BASEDIR/ftp.broadinstitute.org/bundle/pathseq/pathseq_taxonomy.db \
      --output ${TARGET}/`basename ${1}` \
      --scores-output ${TARGET}/`basename ${1} .bam`.pathseq.txt && \rm -f ${INPUTS}/`basename ${1}`
```

## runtime


```{r}

elapsed <- read_tsv("~/iris/midica16/GATK_bundle/elapsed2", col_names = c("jobid", "file", "size", "time"), 
                    col_types = "ccdd")
```

```{r, echo=FALSE, eval=FALSE}
job_type <- tribble(
  ~ jobid,    ~ fs,
  "25", "isilon",
  "252957", "scratch",
  "252916", "scratch",
  "254009", "scratch",
  "253322", "scratch",
  "252957", "scratch",
  "254475", "scratch_strip",
  "256415", "scratch_strip",
  "255609", "scratch_strip_skylake",
  "256568", "scratch_strip_skylake",
  "256569", "scratch_strip_skylake",
  "256569", "scratch_strip_skylake",
  "257084", "scratch_strip_skylake",
  "257085", "scratch_strip_skylake",
  "257205", "scratch_strip_skylake",
  "257206", "scratch_strip_skylake",
  "257595", "scratch_strip_skylake",
  "262424", "scratch_strip_skylake",
  "264743", "scratch_strip_skylake",
  "264887", "scratch_strip_skylake",
  "267399", "scratch_strip_skylake",
  "267401", "scratch_strip_skylake",
  "269000", "scratch_strip_skylake",
  "269307", "scratch_strip_skylake"
)
```

```{r}
elapsed %>% 
  left_join(job_type) %>%
  mutate(id = fs::path_split(file),
         id = map_chr(id, ~ .x[length(.x) - 1])) -> elapsed_type
```



```{r}
tribble(
  ~ size,     ~ time,  ~ jobid, ~ file, ~ fs, 
  1891918473, 374.37,  "25",     "0", "isilon",
   433381365, 1.71,    "25",     "0", "isilon",
 25367705204, 494.75,  "25",     "0", "isilon",
 16592249225, 313.18,  "25",     "0", "isilon",
 45726478933, 1996.28, "25",     "0", "isilon",
   253444075, 1.29,    "25",     "0", "isilon",
 90240311593, 405.15,  "252957", "0", "isilon"
) %>%
  select(jobid, file, size, time, fs) %>%
  bind_rows(elapsed_type) %>%
  filter(!is.na(fs)) %>%
  ggplot(aes(size / 1e9, time / 60, color = fs)) +
  geom_point(size = 2, alpha = 0.5) +
  #geom_line() +
  geom_smooth(method = "lm", formula = y ~ poly(x, 2), se = FALSE) +
  labs(x = "size (Gb)",
       y = "Elapsed time (hours)") +
  scale_y_continuous(labels = scales::comma) +
  theme_bw(14) +
  theme(legend.position = c(0.99, 0.99),
        legend.justification = c(1, 1))
```

```{r}
file_sizes <- read_tsv("~/iris/midica16/GATK_bundle/refs_sizes.txt", col_names = c("file", "type", "size"), col_types = cols())
file_sizes %>%
  mutate(id = fs::path_split(file) %>% map_chr(1)) %>%
  left_join(elapsed_type, by = c("id", "size")) %>%
  mutate(treated = if_else(is.na(time), "no", "yes")) -> file_treated


ggplot(file_treated, aes(x = type, y = size / 1e9, color = treated)) +
  ggbeeswarm::geom_quasirandom(alpha = 0.3) +
  geom_text(data = count(file_treated, treated),
            aes(x = 1.5, y = 85, label = paste("total:", scales::comma(n))), color = "black") +
  geom_text(data = count(file_treated, treated, type),
            aes(y = 75, label = scales::comma(n)), color = "black") +
  facet_wrap(~ treated, labeller = label_both) +
  labs(y = "size (Gb)",
       x = NULL) +
  theme_bw(14) +
  theme(legend.position = "none")
```

improvements:

- working on `scratch` helps a lot. 
- file stripping should also make it faster.
- `skylake` nodes are 15% faster.

### results

using https://gatkforums.broadinstitute.org/gatk/discussion/10913/how-to-run-the-pathseq-pipeline

- score : indicates the amount of evidence that this taxon is present, based on the number of reads that aligned to references in this taxon. This takes into account uncertainty due to ambiguously mapped reads by dividing their weight across each possible hit. It it also normalized by genome length.
- score_normalized : the same as score, but normalized to sum to 100 within each kingdom.
- reads : number of mapped reads (ambiguous or unambiguous)
- unambiguous : number of unambiguously mapped reads
- reference_length : reference length (in bases) if there is a reference assigned to this taxon. Unlike scores, this number is not propagated up the tree, i.e. it is 0 if there is no reference corresponding directly to the taxon. In the above example, the MG1655 strain reference length is only shown in the strain row (4,641,652 bases).



```{r}
rgid <- "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}"
fs::dir_ls(here::here("results", "pathseq"), recurse = TRUE, glob = "*pathseq.txt") %>% 
  #vroom::vroom(col_types = cols_only(taxonomy = "c", type = "c", name = "c", 
  #                                   score = "d", score_normalized = "d",
  #                                   file_id = "c"), id = "file") %>%
  set_names() %>%
  map_dfr(read_tsv, col_types = cols(), .id = "file") %>% 
  mutate(file_id = str_extract(file, rgid)) -> pathseq
vroom::vroom_write(pathseq, "pathseq.tsv.gz")
```

Fetch clinical data using [E. Koncina](https://git-r3lab.uni.lu/myorprog/myorprog/blob/master/analyses/_posts/2018-11-13-myo5b-coverage/myo5b-coverage.Rmd) method using TCGABiolink

```{r, eval=FALSE}
tcga_crc_raw_seq_data_query <- TCGAbiolinks::GDCquery(project = c("TCGA-COAD", "TCGA-READ"),
                                                      data.category = "Sequencing Reads")
file_ids <- tcga_crc_raw_seq_data_query %>% 
  pluck("results", 1) %>% 
  select(file_id, id, cases, experimental_strategy, analysis_workflow_type)

write_csv(file_ids, here::here("data", "file_ids.csv"))
```


Fetch clinical data using [E. Koncina](https://git-r3lab.uni.lu/myorprog/myorprog/blob/master/data-raw/tcga_cdr.xlsx) retrieved from the _Cell_ paper

```{r}
clinical <- readxl::read_excel(here::here("data", "tcga_cdr.xlsx"), col_types = "text") %>%
  select(bcr_patient_barcode, gender, ajcc_pathologic_tumor_stage, histological_grade, tumor_status, new_tumor_event_type)
```

deduplicated samples by E. Koncina (mail to Dominik 2019-05-21)

```{r}
dedup <- read_csv(here::here("data", "tcga_crc_all_sequencing_barcodes.csv")) 
```

A pathseq text output looks like:

```
tax_id	taxonomy	type	name	kingdom	score	score_normalized	reads	unambiguous	reference_length
1581080	root|cellular_organisms|Bacteria|Terrabacteria_group|Firmicutes|Bacilli|Lactobacillales|Streptococcaceae|Streptococcus|Streptococcus_sp._HMSC10E12	species	Streptococcus_sp._HMSC10E12	Bacteria	0.08088000452546394	4.592845231428949E-4	9	0	2347668
1	root	root	root	root	17610.999999999378	199.9999999999988	17611	17611	0
2	root|cellular_organisms|Bacteria	superkingdom	Bacteria	Bacteria	17609.999999999378	99.99999999999888	17611	17609	0
36866	root|cellular_organisms|Bacteria|Proteobacteria|Gammaproteobacteria|Enterobacterales|Enterobacteriaceae|unclassified_Enterobacteriaceae|unclassified_Enterobacteriaceae_(miscellaneous)	no_rank	unclassified_Enterobacteriaceae_(miscellaneous)	Bacteria	0.009259259259259259	5.257955286348234E-5	1	0	0
143361	root|cellular_organisms|Bacteria|Terrabacteria_group|Firmicutes|Clostridia|Clostridiales|Peptostreptococcaceae|Filifactor|Filifactor_alocis	species	Filifactor_alocis	Bacteria	3.405649717514124	0.019339294250506064	7	2	0
```

```{r}


ggplot(pathseq, aes(x = log2(score_normalized))) +
  geom_histogram(bins = 30)

ggplot(pathseq, aes(x = log2(unambiguous))) +
  geom_density() +
  geom_vline(xintercept = log2(5), linetype = "dashed")

file_ids <- read_csv(here::here("data", "file_ids.csv"))
pathseq %>% 
  filter(type == "genus") %>%
  left_join(file_ids, by = c("file_id" = "id")) %>% 
  mutate(bcr_patient_barcode = str_sub(cases, start = 1L, end = 12L)) %>% 
  inner_join(dedup, by = c(cases = "barcode", "experimental_strategy")) %>% 
  left_join(clinical, by = "bcr_patient_barcode") -> pathseq_clinical
  

count(pathseq_clinical, bcr_patient_barcode)

pathseq_clinical %>%
  select(cases, taxonomy:reference_length, experimental_strategy:new_tumor_event_type) %>%
  write_tsv("pathseq_clinical_2635.tsv.gz")

```

Why **tumor status** and **tissue_definition** seems not to match?!


```{r}
pathseq_clinical %>% 
  filter(tissue_definition != "Blood Derived Normal") %>%   #, experimental_strategy == "WXS") %>%
  group_by(bcr_patient_barcode, ajcc_pathologic_tumor_stage, gender, tissue_definition, name) %>%
  mutate(n = n()) %>%
  summarise(score = mean(score)) %>%
  select(bcr_patient_barcode, name, score, tissue_definition, ajcc_pathologic_tumor_stage, gender) %>% 
  spread(name, score, fill = 0) %>% 
  group_by(bcr_patient_barcode) %>% 
  mutate(n = n(),
         id = case_when(n == 1 ~ bcr_patient_barcode,
                        n == 2 ~ paste0(bcr_patient_barcode, tissue_definition),
                        n > 2 ~ "PROBLEM")) %>% 
  select(id, everything())  %>% 
  filter(id != "TCGA-AZ-4684") %>% 
  #filter(tissue_definition == "Solid Tissue Normal") %>% 
  column_to_rownames(var = "id") -> pathseq.df

res.pca <- FactoMineR::PCA(pathseq.df, scale.unit = TRUE, quali.sup = 1:4, graph = FALSE)
factoextra::fviz_screeplot(res.pca)
factoextra::fviz_pca(res.pca, select.var = list(contrib = 10),
                     addEllipses = TRUE, repel = TRUE,
                     #select.ind = list(cos2 = 20), 
                     label = "var", habillage = "tissue_definition")

factoextra::fviz_pca_var(res.pca, select.var = list(cos2 = 0.8))
```




matrix for heatmap

```{r}
pathseq_clinical %>%
  select(cases, name, score_normalized) %>%
  spread(name, score_normalized) %>%
  mutate_at(vars(-cases), replace_na, 0) %>%
  column_to_rownames(var = "cases") %>% 
  as.matrix() -> pathseq_mat
pathseq_mat <- pathseq_mat[, colSums(pathseq_mat) > 10]
pathseq_mat <- t(pathseq_mat)
```

- filtering out Bacteria with less than XX score normalised in total



```{r}
select(pathseq, score_normalized, taxonomy) %>% 
  #head(2000) %>% 
  mutate(taxonomy = str_replace_all(taxonomy, '\\|', '\t')) %>% 
  write_tsv("test.tsv", quote_escape = FALSE, col_names = FALSE )
```



```{r, fig.height=12, fig.width=14}
patient_col <- select(pathseq_clinical, cases, tumor_status, ajcc_grade = ajcc_pathologic_tumor_stage) %>%
  filter(cases %in% colnames(pathseq_mat)) %>% 
  distinct() %>%
  mutate(ajcc_grade = fct_collapse(ajcc_grade,
                              I   = c("Stage I", "Stage IA"),
                              II  = c("Stage II", "Stage IIA", "Stage IIB"),
                              III = c("Stage III", "Stage IIIA", "Stage IIIB"),
                              IV  = c("Stage IV", "Stage IVA", "Stage IVB"),
                              group_other = TRUE),
         tumor_status = if_else(is.na(tumor_status), "[Discrepancy]", tumor_status),
         tumor_status = fct_collapse(tumor_status,
                                     FREE = "TUMOR FREE",
                                     TUMOR = "WITH TUMOR",
                                     group_other = TRUE)) %>%
  column_to_rownames(var = "cases") %>%
  as.data.frame()

ann_colors = list(
    tumor_status = c(FREE = "chartreuse2", TUMOR = "purple", Other = "wheat2"),
    ajcc_grade = c(I = "#F2F0F7", II = "#CBC9E2", III = "#9E9AC8", IV = "#6A51A3", Other = "wheat2"))

pheatmap::pheatmap(pathseq_mat,
                   show_rownames = TRUE,
                   show_colnames = FALSE,
                   color = colorRampPalette(
                     RColorBrewer::brewer.pal(n = 7, name = "YlOrBr"))(100),
                   scale = "none",
                   treeheight_row = 0,
                   treeheight_col = 0,
                   fontsize_row = 7,
                   annotation_col = patient_col,
                   annotation_colors = ann_colors) -> pathseq_heat


pdf(here::here("results", "pathseq_heatmap_2624.pdf"), width = 10, height = 8)
grid::grid.newpage()
grid::grid.draw(pathseq_heat$gtable)
dev.off()

#ggplot(pathseq_clinical, aes(x = file, y = fct_reorder(name, score), fill = score_normalized)) +
#  geom_tile() +
#  scale_fill_viridis_c() +
#  theme_classic(12) +
#  theme(axis.text.x = element_blank()) +
#  labs(x = NULL, y = NULL)
#ggsave("results/pathseq_221bams.pdf", height = 16, width = 10)
```





