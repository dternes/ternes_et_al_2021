---
title: Least-squares means analysis of immune cells quantification in Fuso and Formate
  treated mice
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(tidyverse)
library(lsmeans)
library(ggforce)
```

This script generates a circular heatmap out of the immune cell quantifications performed by flow cytometry. The count fold changes between Fuso treated and control mice are obtained by a least square mean analysis.

The least-squares mean analysis was performed on each combination of tissue and cell type using a linear model which explains the log2 counts by the treatment and takedown (takedown is considered as a block effect).

```{r}
# Generate coordinates alongside a circle for each row in a tibble
add_circle_coordinates <- function(data, x = x, y = y) {
  mutate(data,
         {{x}} := -cos(2 * (row_number() + n() / 4 - 1) * pi / n()),
         {{y}} := sin(2 * (row_number() + n() / 4 - 1) * pi / n())
  )
}
```


```{r}
# read the counts and perform lsm analysis
counts_lsm <- read_csv("data/facs_counts.csv",
                       col_types = cols()) %>% 
  group_by(tissue, cell_label) %>%
  mutate(across(treatment, fct_relevel, "Fuso")) %>% 
  nest() %>%
  mutate(model = map(data, ~lm(log2(absolute_count + 1) ~ treatment + takedown, data = .x)),
         lsm = map(model, lsmeans, specs = "treatment"),
         pairs = map(lsm, pairs),
         pairs = map(pairs, as_tibble)) %>%
  select(-data, -model, -lsm) %>%
  unnest(pairs) %>%
  ungroup() %>% 
  mutate(across(tissue, fct_relevel, "LP", "MLN", "Spleen"))
```


```{r, fig.width = 5, fig.height = 4.5}
# plot the LSM analysis results on a circular heatmap
mutate(counts_lsm,
       significance = if_else(p.value < 0.05, "★", "")) %>%
  arrange(cell_label, tissue) %>%
  rename(p = p.value) %>%
  group_by(cell_label) %>%
  nest() %>%
  ungroup() %>%
  add_circle_coordinates() %>%
  mutate(data = map(data, add_circle_coordinates, x_shift, y_shift)) %>%
  unnest(data) %>% 
  ggplot() +
  geom_circle(data = ~tibble(x = 0, y = 0), aes(x0 = x, y0 = y , r = 1), linetype = "solid", colour = "lightgray") +
  geom_circle(data = ~distinct(.x, cell_label, x, y), aes(x0 = x, y0 = y + 0.02, r= 0.22), fill = "white") +
  geom_point(aes(x = x + x_shift / 10 ,
                 y = y + y_shift / 10,
                 shape = tissue,
                 fill = estimate), size = 5) +
  geom_text(data = ~filter(.x, p < 0.05), aes(x = x + x_shift / 10 , y = y + y_shift / 10, label = significance), size = 3) +
  geom_text(data = ~distinct(.x, x, y, cell_label), aes(x = 1.4 * x , y = 1.4 * y, label = cell_label, hjust = "outward")) +
  coord_fixed() +
  expand_limits(x = c(-1.75, 1.75)) +
  guides(fill = guide_colourbar(title.position = "top", title.hjust = 0.5, label.hjust = 0.5),
         alpha = FALSE,
         shape = guide_legend(ncol = 2, hjust = 0.5 )) +
  
  scale_fill_gradient2(low = "#0571B0",
                       mid = "white",
                       high = "#CA0020",
                       midpoint = 0,
                       limits = c(-2, 2),
                       oob = scales::squish) +
  scale_shape_manual(values = c(LP = 21,
                                Spleen = 22,
                                MLN = 24)) +
  theme_no_axes() +
  theme(legend.position = c(0.5, 0.5),
        legend.box.background = element_rect(fill = NA, colour = NA),
        legend.background = element_rect(fill = NA, colour = NA),
        legend.direction = "horizontal",
        legend.key.width = unit(0.3, "cm"),
        legend.box.just = "center") +
  labs(shape = NULL,
       fill = "log2FC")
```

