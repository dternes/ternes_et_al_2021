EX_alalys(e)	Alanyl-lysine exchange	alalys[e]  <=> 	1	1
EX_alaval(e)	Alanyl-valine exchange	alaval[e]  <=> 	1	1
EX_alaile(e)	Alanyl-isoleucine exchange	alaile[e]  <=> 	1	1
ALALYSabc	L-alanyl-L-lysine transport via ABC system	alalys[e] + atp[c] + h2o[c]  -> adp[c] + alalys[c] + h[c] + pi[c]	0	2	Added for F. nucleatum reconstruction to enable L-analyl-lysine degradation
ALAVALabc	L-alanyl-L-valine transport via ABC system	alaval[e] + atp[c] + h2o[c]  -> adp[c] + alaval[c] + h[c] + pi[c]	0	2	Added for F. nucleatum reconstruction to enable L-analyl-valine degradation
ALAILEabc	L-alanyl-L-isoleucine transport via ABC system	alaile[e] + atp[c] + h2o[c]  -> adp[c] + alaile[c] + h[c] + pi[c]	0	2	Added for F. nucleatum reconstruction to enable L-analyl-isoleucine degradation
ALALYS1c	Hydrolysis of L-alanyl-L-lysine	alalys[c] + h2o[c] + h[c]  <=> ala_L[c] + lys_L[c]	1	2	Added for F. nucleatum reconstruction to enable L-analyl-lysine degradation
ALAVAL1c	Hydrolysis of L-alanyl-L-valine	alaval[c] + h2o[c]  <=> ala_L[c] + val_L[c]	1	2	Added for F. nucleatum reconstruction to enable L-analyl-valine degradation
ALAILE1c	Hydrolysis of L-alanyl-L-isoleucine	alaile[c] + h2o[c]  <=> ala_L[c] + ile_L[c]	1	2	Added for F. nucleatum reconstruction to enable L-analyl-isoleucine degradation
EX_aprut(e)	N-Acetylputrescine exchange	aprut[e]  <=> 	1	1
APRUTtex	N-Acetylputrescine excretion (cytosol to extracellular)	aprut[c] -> aprut[e]	0	0