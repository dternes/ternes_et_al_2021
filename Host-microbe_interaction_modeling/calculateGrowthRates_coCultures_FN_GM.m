solverOK = changeCobraSolver('ibm_cplex','LP');
solverOK = changeCobraSolver('ibm_cplex','QP');

modelIDs={
    'model_FN'
    'model_GM'
    'model_Control'
    'model_T18'
    'modelJoint_FN_GM'
    'modelJoint_T18_FN'
    'modelJoint_T18_GM'
    'modelJoint_T18_FN_GM'
    'modelJoint_Control_FN'
    'modelJoint_Control_GM'
    'modelJoint_Control_FN_GM'
    };
modelBiomasses={
    'Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_biomass0'
    'Gemella_morbillorum_M424_biomass0'
    'Host_biomass_reaction'
    };

modelInfeasible=0;

for j=1:length(modelBiomasses)
    BiomassOptimized{1,j+1}=modelBiomasses{j};
end

for i=1:length(modelIDs)
    i
    BiomassOptimized{i+1,1}=modelIDs{i};
    load(strcat(modelIDs{i},'.mat'));
      % DMEM model
    model=useREFPepDietPlus(model);
    % needed for T18/control model
    model=changeRxnBounds(model,'EX_tag_hs[u]',-10,'l');
        % set the experimentally determined growth rates as
    % constraints-only monocultures
    % data in Excel sheet 'Bacteria_Growth_Rates' and 'FN_growth curve'
    if strcmp(modelIDs{i},'model_FN')
        model=changeRxnBounds(model,'Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_biomass0',0.20476,'b');
        model=changeObjective(model,'Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_biomass0');
    elseif strcmp(modelIDs{i},'model_GM')
        model=changeRxnBounds(model,'Gemella_morbillorum_M424_biomass0',0.248788062,'b');
        model=changeObjective(model,'Gemella_morbillorum_M424_biomass0');
%            model=changeRxnBounds(model,'EX_o2[u]',-0.1,'l');
    elseif strcmp(modelIDs{i},'model_Control') || strcmp(modelIDs{i},'model_T18')
        model=changeObjective(model,'Host_biomass_reaction');
        %     else
        %         model=changeObjective(model,'EX_biomass[c]');
    end
    if strcmp(modelIDs{i},'modelJoint_FN_GM') || strcmp(modelIDs{i},'modelJoint_T18_FN_GM') || strcmp(modelIDs{i},'modelJoint_Control_FN_GM')
        model=changeRxnBounds(model,'Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_biomass0',0.20476,'l');
        model=changeRxnBounds(model,'Gemella_morbillorum_M424_biomass0',0.248788062,'l');
        model.c(find(strcmp(model.rxns,'Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_biomass0')))=1;
        model.c(find(strcmp(model.rxns,'Gemella_morbillorum_M424_biomass0')))=1;
    end
    % assume growth rate of 0.01 for Caco-2
        model=changeRxnBounds(model,'Host_biomass_reaction',0.1,'b');
        %model=changeRxnBounds(model,'Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_biomass0',0.20476,'b');
        %model=changeRxnBounds(model,'Gemella_morbillorum_M424_biomass0',0.248788062,'b');
    solution=solveCobraLP(model);
    % any infeasible solutions?
    if solution.stat ~=1
        modelInfeasible=modelInfeasible+1;
    else
        for j=1:length(modelBiomasses)
            BiomassOptimized{i+1,j+1}=solution.full(strcmp(model.rxns,modelBiomasses{j}));
        end
    end
end
save('BiomassOptimized','BiomassOptimized');

