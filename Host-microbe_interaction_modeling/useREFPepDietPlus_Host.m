function model=useREFPepDietPlus_Host(model)
% Implements the REF diet (fiber-free reference diet) as constraints in the
% in silico models. Added stuff for F.nucleatum

model=changeRxnBounds(model,model.rxns(strmatch('Host_EX_',model.rxns)),0,'l');
%% amino acids
model=changeRxnBounds(model,'Host_EX_gly(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_arg_L(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_cys_L(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_gln_L(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_his_L(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_ile_L(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_leu_L(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_lys_L(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_met_L(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_phe_L(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_ser_L(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_thr_L(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_trp_L(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_tyr_L(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_val_L(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_ala_L(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_glc_D(e)b',-10,'l');
%% other
model=changeRxnBounds(model,'Host_EX_pyr(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_lnlc(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_lipoate(e)b',-1,'l');

%% ions and vitamins: no Host_EXact composition given in the Host_EXperimental medium
% I assume the ones required by the reconstructions
% ions
model=changeRxnBounds(model,'Host_EX_ca2(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_cl(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_so4(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_h2s(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_cobalt2(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_cu2(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_fe2(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_fe3(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_k(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_mg2(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_mn2(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_zn2(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_pi(e)b',-10,'l');
model=changeRxnBounds(model,'Host_EX_h2o(e)b',-10,'l');

%% vitamins
model=changeRxnBounds(model,'Host_EX_fol(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_inost(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_nac(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_ncam(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_pnto_R(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_pydx(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_pydxn(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_ribflv(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_sheme(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_thm(e)b',-1,'l');

% supplemented with hemin and vitamin K
model=changeRxnBounds(model,'Host_EX_pheme(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_mqn7(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_mqn8(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_q8(e)b',-1,'l');

% additional supplements for Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586
%% amino acids
model=changeRxnBounds(model,'Host_EX_pro_L(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_adocbl(e)b',-1,'l');

% peptides
model=changeRxnBounds(model,'Host_EX_alaasp(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_alagln(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_alaglu(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_alagly(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_alahis(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_alaile(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_alaleu(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_alathr(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_alalys(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_alaval(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_glyasn(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_glyasp(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_glycys(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_glygln(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_glyglu(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_glyleu(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_glymet(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_glyphe(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_glypro(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_glytyr(e)b',-1,'l');

% sugars
model=changeRxnBounds(model,'Host_EX_fru(e)b',-1,'l');

% other metabolites
model=changeRxnBounds(model,'Host_EX_lac_L(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_glyc(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_ile(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_aprut(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_5oxpro(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_urea(e)b',-1,'l');

%%
% needed by model-Gemella
model=changeRxnBounds(model,'Host_EX_ade(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_ptrc(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_spmd(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_ocdca(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_gua(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_2dmmq8(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_26dap_M(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_q8(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_ura(e)b',-1,'l');

%% needed for T18/T18_Fn
model=changeRxnBounds(model,'Host_EX_asn_L(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_chol(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_gthrd(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_Lcystin(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_o2s(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_pglyc_hs(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_ps_hs(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_sph1p(e)b',-1,'l');
model=changeRxnBounds(model,'Host_EX_tag_hs(e)b',-1,'l');

end