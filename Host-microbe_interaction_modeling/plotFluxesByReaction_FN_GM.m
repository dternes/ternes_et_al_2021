clear all
% plot the fluxes by reaction
% only the ones with actual and significant flux, otherwise >3000 plots generated!
% still a lot though

load('MinFluxes.mat');
load('MaxFluxes.mat');

for i=2:size(MinFluxes,1)
    % first test if there is any significant non-zero flux through the reaction
    fluxCnt=0;
    for j=7:size(MinFluxes,2)
        if abs(MinFluxes{i,j}) > 0.0001
            fluxCnt=fluxCnt+1;
        end
    end
    for j=7:size(MaxFluxes,2)
        if abs(MaxFluxes{i,j}) > 0.0001
            fluxCnt=fluxCnt+1;
        end
    end
    if fluxCnt>0
        % need to work around a bit to egt both in the same plot with
        % different rectangle colors
        cnt=1;
        for j=7:size(MinFluxes,2)
            minFlux(cnt)=MinFluxes{i,j};
            maxFlux(cnt)=MaxFluxes{i,j};
            cnt=cnt+1;
        end
        % now make a customized figure
        % gives an error if minFlux and maxFlux are not zero but the same value
        errorCnt=0;
        for j=1:length(minFlux)
            if abs(minFlux(j))>0 && abs(maxFlux(j))>0 && maxFlux(j)-minFlux(j)<0.0001
                errorCnt=errorCnt+1;
            end
        end
        if errorCnt==0
            figure
            if strmatch('Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_',MinFluxes{i,1})
                for k = 1:length(minFlux)
                    rectangle('Position', [k-0.2 minFlux(k) 0.4 (maxFlux(k)-minFlux(k))],'FaceColor','g','EdgeColor','k')
                end
                xticks([1 2 3 4 5 6 7 8 9 10 11])
                xticklabels({'FN','GM','Control','T18','FN + GM','T18 + FN','T18 + GM','T18 + FN + GM','Control + FN','Control + GM','Control + FN + GM'});
                xtickangle(90)
                set(gca,'FontSize',12)
                ylabel('Minimal to maximal flux (mmol*gDW-1*hr-1)','fontSize',10)
                h=suptitle(MinFluxes{i,3});
                set(h,'interpreter','none')
                h=title(MinFluxes{i,2});
                set(h,'interpreter','none')
                print('-bestfit',MinFluxes{i,1},'-dpdf','-r300')
                saveas(gcf,MinFluxes{i,1},'eps')
                append_pdfs('Fusobacterium_AllReactions.pdf',strcat(MinFluxes{i,1},'.pdf'));
            elseif strmatch('Gemella_morbillorum_M424_',MinFluxes{i,1})
                for k = 1:length(minFlux)
                    rectangle('Position', [k-0.2 minFlux(k) 0.4 (maxFlux(k)-minFlux(k))],'FaceColor','r','EdgeColor','k')
                end
                xticks([1 2 3 4 5 6 7 8 9 10 11])
                xticklabels({'FN','GM','Control','T18','FN + GM','T18 + FN','T18 + GM','T18 + FN + GM','Control + FN','Control + GM','Control + FN + GM'});
                xtickangle(90)
                set(gca,'FontSize',12)
                ylabel('Minimal to maximal flux (mmol*gDW-1*hr-1)','fontSize',10)
                h=suptitle(MinFluxes{i,3});
                set(h,'interpreter','none')
                h=title(MinFluxes{i,2});
                set(h,'interpreter','none')
                print('-bestfit',MinFluxes{i,1},'-dpdf','-r300')
                saveas(gcf,MinFluxes{i,1},'eps')
                append_pdfs('Gemella_AllReactions.pdf',strcat(MinFluxes{i,1},'.pdf'));
            elseif strmatch('Host_',MinFluxes{i,1})
                for k = 1:length(minFlux)
                    rectangle('Position', [k-0.2 minFlux(k) 0.4 (maxFlux(k)-minFlux(k))],'FaceColor','b','EdgeColor','k')
                end
                xticks([1 2 3 4 5 6 7 8 9 10 11])
                xticklabels({'FN','GM','Control','T18','FN + GM','T18 + FN','T18 + GM','T18 + FN + GM','Control + FN','Control + GM','Control + FN + GM'});
                xtickangle(90)
                set(gca,'FontSize',12)
                ylabel('Minimal to maximal flux (mmol*gDW-1*hr-1)','fontSize',10)
                h=suptitle(MinFluxes{i,3});
                set(h,'interpreter','none')
                h=title(MinFluxes{i,2});
                set(h,'interpreter','none')
                print('-bestfit',MinFluxes{i,1},'-dpdf','-r300')
                saveas(gcf,MinFluxes{i,1},'eps')
                append_pdfs('Host_AllReactions.pdf',strcat(MinFluxes{i,1},'.pdf'));
            elseif strmatch('EX_',MinFluxes{i,1})
                for k = 1:length(minFlux)
                    rectangle('Position', [k-0.2 minFlux(k) 0.4 (maxFlux(k)-minFlux(k))],'FaceColor','m','EdgeColor','k')
                end
                xticks([1 2 3 4 5 6 7 8 9 10 11])
                xticklabels({'FN','GM','Control','T18','FN + GM','T18 + FN','T18 + GM','T18 + FN + GM','Control + FN','Control + GM','Control + FN + GM'});
                xtickangle(90)
                set(gca,'FontSize',12)
                ylabel('Minimal to maximal flux (mmol*gDW-1*hr-1)','fontSize',10)
                h=suptitle(MinFluxes{i,3});
                set(h,'interpreter','none')
                h=title(MinFluxes{i,2});
                set(h,'interpreter','none')
                print('-bestfit',MinFluxes{i,1},'-dpdf','-r300')
                saveas(gcf,MinFluxes{i,1},'eps')
                append_pdfs('Exchange_AllReactions.pdf',strcat(MinFluxes{i,1},'.pdf'));
            end
        end
    end
    % close image afterwards-crashes Matlab otherwise
    close all
end

