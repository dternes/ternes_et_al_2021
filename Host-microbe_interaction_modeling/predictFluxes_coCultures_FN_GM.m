
clear all
% for adding the correct reaction names and formulas
reactionDatabase = readtable('ReactionDatabase.txt', 'Delimiter', 'tab','TreatAsEmpty',['UND. -60001','UND. -2011','UND. -62011'], 'ReadVariableNames', false);
reactionDatabase=table2cell(reactionDatabase);

%initCobraToolbox
solverOK = changeCobraSolver('ibm_cplex','LP');
solverOK = changeCobraSolver('ibm_cplex','QP');
currentDir = pwd;
modelPath=currentDir;
modelIDs={
    'model_FN'
    'model_GM'
    'model_Control'
    'model_T18'
    'modelJoint_FN_GM'
    'modelJoint_T18_FN'
    'modelJoint_T18_GM'
    'modelJoint_T18_FN_GM'
    'modelJoint_Control_FN'
    'modelJoint_Control_GM'
    'modelJoint_Control_FN_GM'
    };

% first collect all of the reactions
Reactions={};
for i=1:length(modelIDs)
    load(strcat(modelPath,'\',modelIDs{i},'.mat'));
    Reactions=vertcat(Reactions,model.rxns);
end
Reactions=unique(Reactions);
for j=1:length(Reactions)
    MinFluxes{j+1,1}=Reactions{j,1};
    MaxFluxes{j+1,1}=Reactions{j,1};
    FluxSpans{j+1,1}=Reactions{j,1};
end
MinFluxes{1,1}='Model_Reaction_ID';
MinFluxes{1,2}='VMH_ID';
MinFluxes{1,3}='Description';
MinFluxes{1,4}='Formula';
MinFluxes{1,5}='Subsystem';
MinFluxes{1,6}='Species';
MaxFluxes{1,1}='Model_Reaction_ID';
MaxFluxes{1,2}='VMH_ID';
MaxFluxes{1,3}='Description';
MaxFluxes{1,4}='Formula';
MaxFluxes{1,5}='Subsystem';
MaxFluxes{1,6}='Species';
FluxSpans{1,1}='Model_Reaction_ID';
FluxSpans{1,2}='VMH_ID';
FluxSpans{1,3}='Description';
FluxSpans{1,4}='Formula';
FluxSpans{1,5}='Subsystem';
FluxSpans{1,6}='Species';

for i=1:length(modelIDs)
    i
    MinFluxes{1,i+6}=modelIDs{i};
    MaxFluxes{1,i+6}=modelIDs{i};
    FluxSpans{1,i+6}=modelIDs{i};
    load(strcat(modelPath,'\',modelIDs{i},'.mat'));
    % DMEM model
    model=useREFPepDietPlus(model);
    % needed for T18/control model
    model=changeRxnBounds(model,'EX_tag_hs[u]',-10,'l');
        % set the experimentally determined growth rates as
    % constraints-only monocultures
    % data in Excel sheet 'Bacteria_Growth_Rates'
    if strcmp(modelIDs{i},'model_FN')
        model=changeRxnBounds(model,'Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_biomass0',0.20476,'b');
        model=changeObjective(model,'Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_biomass0');
    elseif strcmp(modelIDs{i},'model_GM')
        model=changeRxnBounds(model,'Gemella_morbillorum_M424_biomass0',0.248788062,'b');
        model=changeObjective(model,'Gemella_morbillorum_M424_biomass0');
    elseif strcmp(modelIDs{i},'model_Control') || strcmp(modelIDs{i},'model_T18')
        model=changeObjective(model,'Host_biomass_reaction');
        %     else
        %         model=changeObjective(model,'EX_biomass[c]');
    end
    if strcmp(modelIDs{i},'modelJoint_FN_GM') || strcmp(modelIDs{i},'modelJoint_T18_FN_GM') || strcmp(modelIDs{i},'modelJoint_Control_FN_GM')
        model=changeRxnBounds(model,'Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_biomass0',0.01,'l');
        model=changeRxnBounds(model,'Gemella_morbillorum_M424_biomass0',0.01,'l');
        model.c(find(strcmp(model.rxns,'Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_biomass0')))=1;
        model.c(find(strcmp(model.rxns,'Gemella_morbillorum_M424_biomass0')))=1;
    end
    % assume growth rate of 0.01 for T18
        model=changeRxnBounds(model,'Host_biomass_reaction',0.01,'b');
        model=changeRxnBounds(model,'Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_biomass0',0.20476,'b');
        model=changeRxnBounds(model,'Gemella_morbillorum_M424_biomass0',0.248788,'b');
    formulas=printRxnFormula(model);
    currentDir = pwd;
    [minFlux,maxFlux,optsol,ret]=fastFVA(model,90,'max','ibm_cplex');
    %[minFlux, maxFlux, Vmin, Vmax] = fluxVariability(model,90,'max',model.rxns,0,true,'FBA');
    cd(currentDir);
    for j=2:size(MinFluxes,1)
        % remove the species identifiers to get the IDs of just the reactions
        % themselves
        if strmatch('Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_',MinFluxes{j,1})
            MinFluxes{j,2} = regexprep(MinFluxes{j,1},'Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586_','');
            MinFluxes{j,2}=regexprep(MinFluxes{j,2},'\[u\]tr','\(e\)');
            MinFluxes{j,2}=regexprep(MinFluxes{j,2},'IEX_','EX_');
            MinFluxes{j,6}='Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586';
            MaxFluxes{j,2} = MinFluxes{j,2};
            MaxFluxes{j,6}='Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586';
            FluxSpans{j,2} = MinFluxes{j,2};
            FluxSpans{j,6}='Fusobacterium_nucleatum_subsp_nucleatum_ATCC_25586';
        elseif strmatch('Gemella_morbillorum_M424_',MinFluxes{j,1})
            MinFluxes{j,2} = regexprep(MinFluxes{j,1},'Gemella_morbillorum_M424_','');
            MinFluxes{j,2}=regexprep(MinFluxes{j,2},'\[u\]tr','\(e\)');
            MinFluxes{j,2}=regexprep(MinFluxes{j,2},'IEX_','EX_');
            MinFluxes{j,6}='Gemella_morbillorum_M424';
            MaxFluxes{j,2} = MinFluxes{j,2};
            MaxFluxes{j,6}='Gemella_morbillorum_M424';
            FluxSpans{j,2} = MinFluxes{j,2};
            FluxSpans{j,6}='Gemella_morbillorum_M424';
        elseif strmatch('Host_',MinFluxes{j,1})
            MinFluxes{j,2} = regexprep(MinFluxes{j,1},'Host_','');
            MinFluxes{j,2}=regexprep(MinFluxes{j,2},'\[u\]tr','\(e\)');
            MinFluxes{j,2}=regexprep(MinFluxes{j,2},'\(e\)b','\(e\)');
            MinFluxes{j,2}=regexprep(MinFluxes{j,2},'IEX_','EX_');
            MinFluxes{j,6}='Human';
            MaxFluxes{j,2} = MinFluxes{j,2};
            MaxFluxes{j,6}='Human';
            FluxSpans{j,2} = MinFluxes{j,2};
            FluxSpans{j,6}='Human';
        elseif strncmp('EX_',MinFluxes{j,1},3)
            MinFluxes{j,2}=MinFluxes{j,1};
            MinFluxes{j,2}=regexprep(MinFluxes{j,2},'\[u\]','\(e\)');
            MinFluxes{j,2}=regexprep(MinFluxes{j,2},'glc_D','glc');
            MinFluxes{j,2}=regexprep(MinFluxes{j,2},'adocbl','adpcbl');
            MinFluxes{j,2}=regexprep(MinFluxes{j,2},'sbt_D','sbt-d');
            MinFluxes{j,6}='Exchange';
            MaxFluxes{j,2}=MinFluxes{j,2};
            MaxFluxes{j,6}='Exchange';
            FluxSpans{j,2}=MinFluxes{j,2};
            FluxSpans{j,6}='Exchange';
        end
        % fill in the reaction information from the database, otherwise
        % from the model
        findRxn=find(strcmp(reactionDatabase(:,1),MinFluxes{j,2}));
        if ~isempty(findRxn)
            MinFluxes{j,3}=reactionDatabase{findRxn,2};
            MinFluxes{j,4}=reactionDatabase{findRxn,3};
            MaxFluxes{j,3}=reactionDatabase{findRxn,2};
            MaxFluxes{j,4}=reactionDatabase{findRxn,3};
            FluxSpans{j,3}=reactionDatabase{findRxn,2};
            FluxSpans{j,4}=reactionDatabase{findRxn,3};
        else
            findRxn=find(strcmp(model.rxns,MinFluxes{j,1}));
            if ~isempty(findRxn)
            MinFluxes{j,3}=model.rxnNames{findRxn};
            MinFluxes{j,4}=formulas{findRxn};
            MaxFluxes{j,3}=model.rxnNames{findRxn};
            MaxFluxes{j,4}=formulas{findRxn};
            FluxSpans{j,3}=model.rxnNames{findRxn};
            FluxSpans{j,4}=formulas{findRxn};
            end
        end
        findRxn=find(strcmp(model.rxns,MinFluxes{j,1}));
        if ~isempty(findRxn)
            MinFluxes{j,5}=model.subSystems{findRxn};
            MaxFluxes{j,5}=model.subSystems{findRxn};
            FluxSpans{j,5}=model.subSystems{findRxn};
            % now fill in the fluxes
            MinFluxes{j,i+6}=minFlux(findRxn);
            MaxFluxes{j,i+6}=maxFlux(findRxn);
            if maxFlux(findRxn) > 0.0000000001 && minFlux(findRxn) > 0.0000000001
                FluxSpans{j,i+6}=maxFlux(findRxn)-minFlux(findRxn);
            elseif maxFlux(findRxn) > 0.0000000001 && minFlux(findRxn) <-0.0000000001
                FluxSpans{j,i+6}=maxFlux(findRxn) + abs(minFlux(findRxn));
            elseif maxFlux(findRxn) < -0.0000000001 && minFlux(findRxn) <-0.0000000001
                FluxSpans{j,i+6}=abs(minFlux(findRxn)) - abs(maxFlux(findRxn));
            elseif maxFlux(findRxn) > 0.0000000001 && abs(minFlux(findRxn)) <0.0000000001
                FluxSpans{j,i+6}=maxFlux(findRxn);
            elseif minFlux(findRxn) < -0.0000000001 && abs(maxFlux(findRxn)) <0.0000000001
                FluxSpans{j,i+6}=abs(minFlux(findRxn));
            elseif abs(maxFlux(findRxn)) < 0.0000000001 && abs(minFlux(findRxn)) <0.0000000001
                FluxSpans{j,i+6}=0;
            end
        else
            MinFluxes{j,i+6}=0;
            MaxFluxes{j,i+6}=0;
            FluxSpans{j,i+6}=0;
        end
    end
end
save('MinFluxes.mat','MinFluxes');
save('MaxFluxes.mat','MaxFluxes');
save('FluxSpans.mat','FluxSpans');
xlswrite('Multi_simulation_results.xls', FluxSpans, 1)
xlswrite('Multi_simulation_results.xls', MinFluxes, "MinFluxes")
xlswrite('Multi_simulation_results.xls', MaxFluxes, "MaxFluxes")
